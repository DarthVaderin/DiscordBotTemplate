package de.darthvaderin.discordbottemplate;

import java.awt.Color;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.*;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 *
 * @author DarthVaderin
 */
public class AnnotationListener {
     private String botprefix = "<";
     
     @EventSubscriber
     public void kannstdunennenwieduwillst(MessageReceivedEvent event) throws MissingPermissionsException, RateLimitException, DiscordException{
        IMessage message = event.getMessage();
        if (message.getContent().startsWith(botprefix)) {
            String command = message.getContent().replaceFirst(botprefix, ""); // Remove prefix
            String[] args = command.split(" ");
/*Mit Prefix*/            
            if(args[0].equalsIgnoreCase("help")){
                message.reply("Hier kommt Hilfe");
                //Weitere Möglichkeit, was man machen könnte:
                IRole r;
                try{ /*damit die Rollen nicht mehrfach erstellt werden, versucht man erst, eine bestehende testrolle zu nehmen*/
                    r = message.getGuild().getRolesByName("testrolle").get(0);
                }
                catch(Exception e){
                    r = message.getGuild().createRole();
                    r.changeName("testrolle");
                    r.changeColor(Color.yellow);
                }
                message.getAuthor().addRole(r);
            }
/*Ohne Prefix*/
        }else{
            String[] args = message.getContent().split(" ");
            if(args.length>1){ //Sonst wird ArrayOutOfBoundsException geworfen, aber eigentlich nicht unbedingt nötig (nur sehr viel schöner xD)
                if(args[0].equalsIgnoreCase("Ich")&&args[1].equalsIgnoreCase("brauche")&&args[2].equalsIgnoreCase("Hilfe")){
                    message.reply("Hier hast du deine Hilfe");
                }
            }
        }
     }
     
/*weiteres mögliches Event*/
     @EventSubscriber
     public void andereMethode(UserJoinEvent event) throws RateLimitException, DiscordException, MissingPermissionsException{
         event.getUser().getOrCreatePMChannel().sendMessage("Mal ganz privat: Herzlich Willkommen auf diesem neuen Server.");
     }
     
}
